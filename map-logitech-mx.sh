#!/usr/bin/env bash
set -o errexit

# debug
#set -x

# variables
declare _MYUSER="${USER}"
declare _MYDIR=""
declare _BUDIR=""
declare _CONFDIR=""
declare _SRC=""
declare _DST="${HOME}/.xbindkeysrc"
declare VERBOSITY="0"
declare _SOURCE=""
declare _OSVENDOR=""
declare _PACKAGEMGR=""
declare _PKGCHCKCMD=""
declare _LSBREL=""

# find the directory the script is residing in
_SOURCE=${BASH_SOURCE[0]}
while [ -L "$_SOURCE" ];
do
    # resolve $_SOURCE until the file is no longer a symlink
    DIR=$( cd -P "$( dirname "$_SOURCE" )" >/dev/null 2>&1 && pwd )
    _SOURCE=$(readlink "$_SOURCE")
    [[ $_SOURCE != /* ]] && _SOURCE=$DIR/$_SOURCE # if $_SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done

_MYDIR=$( cd -P "$( dirname "$_SOURCE" )" >/dev/null 2>&1 && pwd )
_BUDIR="${_MYDIR}/backup"
_CONFDIR="${_MYDIR}/cfg"

# functions
function err ()
{
    # errors will always get printed
    print_msg "0" "ERROR: ${1}, exiting!"
    exit 1
}

function notice ()
{
    print_msg "1" "NOTICE: ${1}"
}

function inf ()
{
    print_msg "2" "INFO: ${1}"
}

function print_msg ()
{
    if [ "$VERBOSITY" -eq "${1}" ] || [ "$VERBOSITY" -gt "${1}" ]
    then
        echo "${2}"
    fi
}

function chklsbrelease ()
{
    if command -v lsb-release &> /dev/null
    then
        _LSBREL="$( command -v lsb-release )"
    elif command -v lsb_release &> /dev/null
    then
        _LSBREL="$( command -v lsb_release )"
    else
        err "Package 'lsb-release' required but missing"
    fi
}

function chkplatform ()
{
    _OSVENDOR="$( ${_LSBREL} -is )"

    case $_OSVENDOR in
        openSUSE)
            _PKGCHCKCMD="$( command -v rpm ) -q"
            ;;

        Debian | Kali)
            _PKGCHCKCMD="$( command -v dpkg-query ) -W"
            ;;

        *)
            err "Your operating system (${_OSVENDOR}) is not supported"
            ;;
    esac

    inf "Package Check Command set to '${_PKGCHCKCMD}'."
}

function chkprerequisites ()
{
    # check for required packages
    for PACKAGE in xautomation xbindkeys xdotool
    do
        inf "Checking if package '${PACKAGE}' is installed."
        eval "${_PKGCHCKCMD}" "${PACKAGE}" > /dev/null 2>&1 || err "Package '${PACKAGE}' is not installed"
    done

    # check for config and backup directories
    for DIRECTORY in "$_BUDIR" "$_CONFDIR"
    do
        inf "Checking if directory '${DIRECTORY}' exists."
        [ -d "${DIRECTORY}" ] || err "Directory ${DIRECTORY} does not exist"
    done

    inf "checking if ${_SRC} does exist."
    if [ ! -f "${_SRC}" ]
    then
        err "${_SRC} does not exist"
    fi
}

function backup ()
{
    if [ -f "$_DST" ]
    then
        inf "Attempting to create a backup of ${_DST} in ${_BUDIR}."
        /bin/cp -rf "$_DST" "${_BUDIR}/xbindkeysrc.backup" || err "Could not create backup."
    else
        notice "${_DST} does not exist, no backup created."
    fi
}

function restartxbk ()
{
    if /usr/bin/pgrep -U "${_MYUSER}" xbindkeys > /dev/null 2>&1
    then
        inf "Attempting to kill existing instance of xbindkeys."
        /usr/bin/pkill -U "${_MYUSER}" xbindkeys > /dev/null 2>&1 || err "Could not kill xbindkeys"
    else
        notice "No instance of xbindkeys has been found."
    fi
    inf "Attempting to run xbindkeys."
    /usr/bin/xbindkeys || err "Could not run xbindkeys"
}

function cpyconfig ()
{
    # Copy the specified config into the users home.
    notice "Copying ${_SRC} to ${_DST}."
    /bin/cp -rf "${_SRC}" "${_DST}" || err "couldn't copy ${_SRC} to ${_DST}"
}

function testargument ()
{
    if [ ! -f "${_SRC}" ]
    then
        # given argument is not a file in CFG directory
        err "The specified configuration file ${_SRC} does not exist"
    fi
}

function main ()
{
    # check if lsb-release is installed
    chklsbrelease

    # check the platform we are running on
    chkplatform

    # check the prerequisites
    chkprerequisites

    # try to create a backup
    backup

    # copy the config
    cpyconfig

    # Start/Restart xbindkeys if the config has been copied successfully
    restartxbk
}

# handle argument(s)
case $# in
    0)
        # set $_SRC to default config
        _SRC="$_CONFDIR/default.cfg"
        ;;
    1)
        # set $_SRC according to the given argument
        _SRC="${_CONFDIR}/${1}.cfg"

        # test if the given argument is an existing file
        testargument
        ;;

    *)
        # more than one argument
        err "Only one argument allowed"
        ;;
esac

# run the script
main

# exit with status 0
exit 0