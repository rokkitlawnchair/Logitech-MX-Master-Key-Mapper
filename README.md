# Linux Mappings for Logitech MX Master

This script will automate switching and installing key mappings for Logitech MX Master
mouse series.

The script first checks if the required prerequisites are installed and prints an
error message if not.

Then it generates the file ```.xbindkeysrc``` in the users home directory and calls
```xbindkeys``` to configure and activate the mapping.

[ArchLinux wiki](https://wiki.archlinux.org/index.php/Logitech_MX_Master) has been used for reference.




## Compatibility
### Tested Operating Systems (2023/01/06)
+ openSUSE Tumbleweed
+ Kali-rolling

It's been running on Ubuntu some while ago - but I don't use that platform anymore.


### Tested Hardware
+ [Logitech MX Master 2S](https://support.logi.com/hc/en-us/articles/360023459853-MX-MASTER-2S-WIRELESS-MOUSE) (M-R0052)
+ [Logitech MX Master 3s for Business](https://prosupport.logi.com/hc/en-us/articles/8733879560727-Getting-Started-MX-Master-3s-for-Business) (MR0077)


## Prerequisites
Requirements to run this script:
+ [lsb-release](https://refspecs.linuxfoundation.org/LSB_3.0.0/LSB-PDA/LSB-PDA/lsbrelease.html)
+ [xbindkeys](https://www.nongnu.org/xbindkeys/)
+ [xdotool](https://github.com/jordansissel/xdotool)
+ [xautomation](https://www.hoopajoo.net/projects/xautomation.html)


## Installing / Running the Script
For example:
```
# installing the prerequisites
#
# as root (on openSUSE):
#
zypper in lsb-release xautomation xbindkeys xdotool
# as root (on Kali):
apt install lsb-release xautomation xbindkeys xdotool


#
# as user
#
mkdir -pv "${HOME}"/repos/codeberg/
git clone git@codeberg.org:rokkitlawnchair/Logitech-MX-Master-Key-Mapper.git "${HOME}"/repos/codeberg/Logitech-MX-Master-Key-Mapper
chmod -v u+x "${HOME}"/repos/codeberg/Logitech-MX-Master-Key-Mapper/map-logitech-mx.sh

# in case you have ~/.local/bin in your $PATH:
ln -v -s "${HOME}"/repos/codeberg/Logitech-MX-Master-Key-Mapper/map-logitech-mx.sh "${HOME}"/.local/bin/map-logitech-mx
map-logitech-mx

# otherwise:
"${HOME}"/repos/codeberg/Logitech-MX-Master-Key-Mapper/map-logitech-mx.sh
```

To load other configurations, just place them into the cfg-directory (FILENAME.cfg) and load them as follows:
```
map-logitech-mx <FILENAME without SUFFIX>

# e.g. for l4d2.cfg:
map-logitech-mx l4d2
```


## Key Mapping
The default mapping is:

| Key | Mapping |
| --- | --- |
| `>Thumb Wheel Up` | raise volume | 
| `>Thumb Wheel Down` | lower volume |
| `>Forward Thumb Key (lower on 2S, left on 3s)` | page up |
| `>Backward Thumb Key (upper on 2S, right on 3s)` | page down |

But you can define/load your own mappings.

![Key mappings](https://www.nanoscopic.de/wp-content/uploads/2020/11/keys.png)


## Credits
+ This script is based on the work of Brian Lam. [Brians Logitech MX Master Key Mapper Linux source repository at github](https://github.com/Brian-Lam/Logitech-MX-Master-Key-Mapper-Linux).
+ The script also contains code of [Dave Dopson](https://stackoverflow.com/users/407731/dave-dopson) which I have found at [stackoverflow](https://stackoverflow.com/questions/59895/how-do-i-get-the-directory-where-a-bash-script-is-located-from-within-the-script/246128#246128).


### Image Credits
Photo by [Yasin Hasan](https://unsplash.com/@yasin) on [Unsplash](https://unsplash.com/photos/RwLX8q5Y2qQ/), [license](https://unsplash.com/license)